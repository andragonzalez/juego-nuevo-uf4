$(document).ready(function(){
  console.log("Welcome to click fight");

  $("#plataforma").on("click", function() {

    let valorActual = $("#number").text();
    $("#number").text(parseInt(valorActual)+1);

    if (valorActual % 5 == 0) {

        $( "#number" ).animate({
          opacity: 0.5,
          width: $("#number").width() + 5,
          height: $("#number").height() + 5,
          "background-color": getRandomColor()
        });
    }
  });

});


function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
