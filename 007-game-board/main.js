/*$(document).ready(function(){
  console.log("Welcome to the Game");
  var player = $("#player"); // Element player
  var ball = $("#ball"); // Element ball
  var scenario = $("#scenario"); // Element scenario
  var dx = 5; // Precisión parameters
  var dy = 5; // Precisión parameters
  captureKeys();

/* INICIO función para controlar al jugador */


  /*function moveLeft() {
    player.css({left: player.position().left - dx});
    //player.animate({ left: player.position().left - dx}, "fast");
  }

  function moveRight() {
    player.css({left: player.position().left + dx});
    //player.animate({ left: player.position().left + dx}, "fast");

  }

  function moveDown() {
    player.css({top: player.position().top + dy});
    //player.animate({ top: player.position().top + dy}, "fast");
  }

  function moveUp() {
    player.css({top: player.position().top - dy});
    //player.animate({ top: player.position().top - dy}, "fast");
  }

  function jump() {
    player.css({top: player.position().top - 8*dy});
  // let startPoint = player.position();
    //player.animate({ top: startPoint.top - 8*dy}, "fast");
     //player.animate({ top: startPoint.top - 5*dy}, "fast");
     //player.animate({ top: startPoint.top - 3*dy}, "fast");
     //player.animate({ top: startPoint.top - dy}, "fast");
     //player.animate({ top: startPoint.top}, "fast");

  }

  function fire() {
    $('<div />').addClass("bullet").appendTo(player);
  }

/* FIN función para controlar al jugador */


/* INICIO función para capturar las teclas */
  /*function captureKeys() {
    $("body").keypress(function(){
      if (event.which == 97) {
        // A
        moveLeft();
      }

      if (event.which == 100) {
        // D
        moveRight();
      }

      if (event.which == 115) {
        // S
        moveDown();
      }

      if (event.which == 119) {
        // W
        moveUp();

      }

      if (event.which == 102) {
        // F
        fire();
      }

      if (event.which == 32) {
        // SPACE
    //    jump();
          bounce();
      }


      $( "#log" ).prepend( event.type + ": " +  event.which + "<br/>");

      return false;

    });
  }

/* FIN función para capturar las teclas */




/*});

// https://stackoverflow.com/questions/7298507/move-element-with-keypress-multiple
/* const Player = {
  el: document.getElementById('player'),
  x: 0,
  y: 0,
  speed: 2,
  move() {
    this.el.style.transform = `translate(${this.x}px, ${this.y}px)`;
  }
};

const K = {
  fn(ev) {
    const k = ev.which;
    if (k >= 37 && k <= 40) {
      ev.preventDefault();
      K[k] = ev.type === "keydown"; // If is arrow
    }
  }
};

const update = () => {
  let dist = K[38] && (K[37] || K[39]) || K[40] && (K[37] || K[39]) ? 0.707 : 1;
  dist *= Player.speed;
  if (K[37]) Player.x -= dist;
  if (K[38]) Player.y -= dist;
  if (K[39]) Player.x += dist;
  if (K[40]) Player.y += dist;
  Player.move();
}

document.addEventListener('keydown', K.fn);
document.addEventListener('keyup', K.fn);

(function engine() {
  update();
  window.requestAnimationFrame(engine);
}());
*/












var canvas = document.getElementById('game');
var context = canvas.getContext('2d');

var grid = 16;
var count = 0;

var snake = {
  x: 200,
  y: 200,

  // snake velocity. moves one grid length every frame in either the x or y direction
  dx: grid,
  dy: 0,

  // keep track of all grids the snake body occupies
  cells: [],

  // length of the snake. grows when eating an apple
  maxCells: 4
};
var apple = {
  x: 320,
  y: 320,
};

// get random whole numbers in a specific range
// @see https://stackoverflow.com/a/1527820/2124254
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

// game loop
function loop() {
  requestAnimationFrame(loop);

  // slow game loop to 15 fps instead of 60 (60/15 = 4)
  if (++count < 4) {
    return;
  }

  count = 0;
  context.clearRect(0,0,canvas.width,canvas.height);

  // move snake by it's velocity
  snake.x += snake.dx;
  snake.y += snake.dy;

  // wrap snake position horizontally on edge of screen
  if (snake.x < 0) {
    snake.x = canvas.width - grid;
  }
  else if (snake.x >= canvas.width) {
    snake.x = 0;
  }

  // wrap snake position vertically on edge of screen
  if (snake.y < 0) {
    snake.y = canvas.height - grid;
  }
  else if (snake.y >= canvas.height) {
    snake.y = 0;
  }

  // keep track of where snake has been. front of the array is always the head
  snake.cells.unshift({x: snake.x, y: snake.y});

  // remove cells as we move away from them
  if (snake.cells.length > snake.maxCells) {
    snake.cells.pop();
  }

  // draw apple
  context.fillStyle = 'red';
  context.fillRect(apple.x, apple.y, grid-1, grid-1);

  // draw snake one cell at a time
  context.fillStyle = 'white';
  snake.cells.forEach(function(cell, index) {

    // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
    context.fillRect(cell.x, cell.y, grid-1, grid-1);

    // snake ate apple
    if (cell.x === apple.x && cell.y === apple.y) {
      snake.maxCells++;

      // canvas is 400x400 which is 25x25 grids
      apple.x = getRandomInt(0, 25) * grid;
      apple.y = getRandomInt(0, 25) * grid;
    }

    // check collision with all cells after this one (modified bubble sort)
    for (var i = index + 1; i < snake.cells.length; i++) {

      // snake occupies same space as a body part. reset game
      if (cell.x === snake.cells[i].x && cell.y === snake.cells[i].y) {
        snake.x = 160;
        snake.y = 160;
        snake.cells = [];
        snake.maxCells = 4;
        snake.dx = grid;
        snake.dy = 0;

        apple.x = getRandomInt(0, 25) * grid;
        apple.y = getRandomInt(0, 25) * grid;
      }
    }
  });
}

// listen to keyboard events to move the snake
document.addEventListener('keydown', function(e) {
  // prevent snake from backtracking on itself by checking that it's
  // not already moving on the same axis (pressing left while moving
  // left won't do anything, and pressing right while moving left
  // shouldn't let you collide with your own body)

  // left arrow key
  if (e.which === 37 && snake.dx === 0) {
    snake.dx = -grid;
    snake.dy = 0;
  }
  // up arrow key
  else if (e.which === 38 && snake.dy === 0) {
    snake.dy = -grid;
    snake.dx = 0;
  }
  // right arrow key
  else if (e.which === 39 && snake.dx === 0) {
    snake.dx = grid;
    snake.dy = 0;
  }
  // down arrow key
  else if (e.which === 40 && snake.dy === 0) {
    snake.dy = grid;
    snake.dx = 0;
  }
});

requestAnimationFrame(loop);
